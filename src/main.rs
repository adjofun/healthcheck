extern crate hyper;

use std::env::Args;
use url::Url;
use hyper::{Uri, Client, Response};
use tokio::time::Duration;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let conf = Conf::from_command_line(std::env::args());
    if conf.is_err() {
	println!("{}", conf.unwrap_err());
	return Ok(());
    }
    let conf = conf.unwrap();
    
    let client = Client::new();
    loop {
	let http_resp = client.get(conf.get_uri()).await?;
	let check_result = CheckResult::from_http_resp(http_resp);
	println!("{}", check_result.report(&conf.url));
	tokio::time::sleep(conf.repeat_interval).await;
    }
}

#[derive(Debug)]
struct Conf {
    repeat_interval: Duration,
    url: Url
}
impl Conf {
    fn from_command_line(args: Args) -> Result<Conf, String> {
	let arg_vec: Vec<String> = args.collect();
	let secs = arg_vec[1].parse().expect("First argument should be a positive number");
	let repeat_interval = Duration::from_secs(secs);

	Url::parse(&arg_vec[2])
	    .map(|url| Conf{repeat_interval, url})
	    .map_err(|_err| "URL parsing error".to_string())
    }

    fn get_uri(&self) -> Uri {
	self.url.to_string().parse().unwrap()
    }
}


struct CheckResult {
    is_success: bool,
    status_code: u16
}
impl CheckResult {
    fn from_http_resp<T>(http_resp: Response<T>) -> CheckResult {
	let is_success = http_resp.status().is_success();
	let status_code = http_resp.status().as_u16();
	CheckResult{is_success, status_code}
    }

    fn report(&self, url: &Url) -> String {
	format!("Checking '{}'. Result: {}"
		, url
		, self.to_string()
	)
    } 
}
impl ToString for CheckResult {
    fn to_string(&self) -> String {
	format!("{}({})"
		, if self.is_success {"OK"} else {"ERR"}
		, self.status_code
	)
    }
}

